var startState = {
    preload: function() {
        var loadingLabel = game.add.text(game.width/2, game.height/2, 'loading...', {font: '30px Arial', fill:'#ffffff'});
        loadingLabel.anchor.setTo(0.5, 0.5);

        /*var progressBar = game.add.sprite(game.width/2, game.height/2+50, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);*/

        game.load.image('plane1', 'assets/plane1.png');
        game.load.image('plane2', 'assets/plane2.png');
        game.load.image('enemy', 'assets/enemy.png');
        game.load.image('boss', 'assets/boss.png');
        game.load.image('beam1', 'assets/beam1.png');
        game.load.image('beam2', 'assets/beam2.png');
        game.load.image('bullet1', 'assets/bullet1.png');
        game.load.image('bullet2', 'assets/bullet2.png');
        game.load.image('laser', 'assets/laser.png');
        game.load.image('pixel1', 'assets/pixel1.png');
        game.load.image('pixel2', 'assets/pixel2.png');
        game.load.image('help', 'assets/help.png');
        game.load.image('auto', 'assets/auto.png');
        game.load.image('special', 'assets/special.png');
        game.load.image('menubg', 'assets/menubg.jpg');
        game.load.image('background', 'assets/background.jpg');
        game.load.image('pause', 'assets/pause.png');
        game.load.image('sound', 'assets/sound.png');
        game.load.audio('music', 'assets/overworld.mp3');
        game.load.audio('beamSound', 'assets/beam.mp3');
    },

    create: function() {
        game.state.start('menu');
    }
};