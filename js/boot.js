var bootState = {
    preload: function() {
        game.load.image('progressBar', 'assets/bar.png');
    },

    create: function() {
        game.state.start('start');
    }
};