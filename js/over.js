var overState = {
    create: function() {
        this.bg = game.add.tileSprite(0, 0, 800, 600, 'background');

        var overLabel = game.add.text(game.width/2, 200, 'Game Over', {font: '80px Arial', fill:'#ffffff'});
        overLabel.anchor.setTo(0.5, 0.5);

        var restartLabel = game.add.text(game.width/2, 280, 'Press R to restart', {font: '40px Arial', fill:'#ffffff'});
        restartLabel.anchor.setTo(0.5, 0.5);

        var quitLabel = game.add.text(game.width/2, 330, 'Press Q to quit', {font: '40px Arial', fill:'#ffffff'});
        quitLabel.anchor.setTo(0.5, 0.5);

        this.restartKey = game.input.keyboard.addKey(Phaser.Keyboard.R);
        this.quitKey = game.input.keyboard.addKey(Phaser.Keyboard.Q);
    },

    update: function() {
        this.bg.tilePosition.y += 2;

        if(this.restartKey.isDown) {
            game.state.start('play');
        } else if(this.quitKey.isDown) {
            game.state.start('menu');
        }
    }
};