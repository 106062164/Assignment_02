var menuState = {
    create: function() {
        game.add.image(0, 0, 'menubg');

        var nameLabel = game.add.text(200, 75, 'Raiden', {font: '80px Arial', fill:'#ffffff'});
        nameLabel.anchor.setTo(0.5, 0.5);

        var startLabel = game.add.text(188, 120, '(Click Enter to Play)', {font: '25px Arial', fill:'#ffffff'});
        startLabel.anchor.setTo(0.5, 0.5);

        var moveLabel = game.add.text(70, 350, 'Up, down, left, right to move', {font: '25px Arial', fill:'#ffffff'});

        var shootLabel = game.add.text(70, 400, 'Space to shoot', {font: '25px Arial', fill:'#ffffff'});

        var ultimateLabel = game.add.text(70, 450, 'Shift to use ultimate skill', {font: '25px Arial', fill:'#ffffff'});

        var specialLabel = game.add.text(70, 500, 'Ctrl to use special beam', {font: '25px Arial', fill:'#ffffff'});

        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this);
    },

    start: function() {
        game.state.start('play');
    }
};