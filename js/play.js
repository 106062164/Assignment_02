var playState = {
    create: function() {
        beamTime = 0;
        bulletTime = 0;
        bombTime = 0;
        helpTime = 0;
        supportTime = 0;
        boosHP = 500;
        livingEnemies = [];
        livingTargets = [];
        livingBosses = [];
        level = 1;
        life = 20;
        SP = 0;
        burst = 0;
        score = 0;
        isAuto = 0;
        isHelp = 0;
        isSpecial = 0;

        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.bg = game.add.tileSprite(0, 0, 800, 600, 'background');

        player1 = game.add.sprite(game.width/2, 550, 'plane1');
        player1.scale.setTo(1.1, 1.1);
        player1.anchor.setTo(0.5, 0.5);
        player1.enableBody = true;
        game.physics.enable(player1, Phaser.Physics.ARCADE);

        helps = game.add.group();
        helps.enableBody = true;
        helps.physicsBodyType = Phaser.Physics.ARCADE;
        helps.createMultiple(1, 'plane2');

        supports = game.add.group();
        supports.enableBody = true;
        supports.physicsBodyType = Phaser.Physics.ARCADE;
        supports.createMultiple(1, 'plane2');

        beams = game.add.group();
        beams.enableBody = true;
        beams.physicsBodyType = Phaser.Physics.ARCADE;
        beams.createMultiple(100, 'beam1');

        lasers = game.add.group();
        lasers.enableBody = true;
        lasers.physicsBodyType = Phaser.Physics.ARCADE;
        lasers.createMultiple(1, 'laser');

        sbeams = game.add.group();
        sbeams.enableBody = true;
        sbeams.physicsBodyType = Phaser.Physics.ARCADE;
        sbeams.createMultiple(1, 'beam2');

        this.cursor = game.input.keyboard.createCursorKeys();
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.shiftKey = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);
        this.ctrlKey = game.input.keyboard.addKey(Phaser.Keyboard.CONTROL);

        this.enemyNum = 0;
        this.enemyPos = 40;
        this.enemyCom = 0;
        enemies = game.add.group();
        enemies.enableBody = true;
        enemies.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.createMultiple(8, 'enemy');

        this.boss = 0;
        bosses = game.add.group();
        bosses.enableBody = true;
        bosses.physicsBodyType = Phaser.Physics.ARCADE;
        bosses.createMultiple(1, 'boss');

        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(100, 'bullet1');

        bombs = game.add.group();
        bombs.enableBody = true;
        bombs.physicsBodyType = Phaser.Physics.ARCADE;
        bombs.createMultiple(100, 'bullet2');

        pause = game.add.sprite(775, 25, 'pause');
        pause.anchor.setTo(0.5, 0.5);
        pause.scale.setTo(0.2, 0.2);
        pause.inputEnabled = true;

        sound = game.add.sprite(725, 25, 'sound');
        sound.anchor.setTo(0.5, 0.5);
        sound.scale.setTo(0.2, 0.2);
        sound.inputEnabled = true;

        auto = game.add.sprite(40, 200, 'auto');
        auto.anchor.setTo(0.5, 0.5);
        auto.scale.setTo(0.1, 0.1);
        auto.enableBody = true;
        game.physics.enable(auto, Phaser.Physics.ARCADE);

        help = game.add.sprite(40, 300, 'help');
        help.anchor.setTo(0.5, 0.5);
        help.scale.setTo(0.1, 0.1);
        help.enableBody = true;
        game.physics.enable(help, Phaser.Physics.ARCADE);

        special = game.add.sprite(40, 400, 'special');
        special.anchor.setTo(0.5, 0.5);
        special.scale.setTo(0.2, 0.2);
        special.enableBody = true;
        game.physics.enable(special, Phaser.Physics.ARCADE);

        this.music = game.add.audio('music');
        this.music.play();
        this.music.loop = true;
        this.music.volume = 1;
        this.beamSound = game.add.audio('beamSound');
        this.beamSound.volume = 1;

        this.lifeLabel = game.add.text(game.width/2, 585, 'Lives : ' + life, {font: '30px Arial', fill:'#ffffff'});
        this.lifeLabel.anchor.setTo(0.5, 0.5);
        this.levelLabel = game.add.text(game.width/2, 20, 'Level : ' + level, {font: '30px Arial', fill:'#ffffff'});
        this.levelLabel.anchor.setTo(0.5, 0.5);
        this.SPLabel = game.add.text(690, 585, 'Ultimate : ' + level + '%', {font: '30px Arial', fill:'#ffffff'});
        this.SPLabel.anchor.setTo(0.5, 0.5);
        this.scoreLabel = game.add.text(90, 585, 'Score : ' + score, {font: '30px Arial', fill:'#ffffff'});
        this.scoreLabel.anchor.setTo(0.5, 0.5);
    },

    update: function() {
        this.bg.tilePosition.y += 2;

        pause.events.onInputDown.add(this.pause, this);
        sound.events.onInputDown.add(this.mute, this);

        if(player1.alive) {
            this.movePlayer();
            this.shootBeam();
            this.ultimateSkill();

            if(isHelp == 1) {
                phelp.reset(player1.x+50, player1.y+50);
                support.reset(player1.x-50, player1.y+50);
                this.helpShoot();
                this.supportShoot();
            }

            if(isSpecial == 1) {
                this.specialBeam();
            }
        }

        if(level == 0 || (this.enemyCom == 0 && level%5 != 0)) {
            this.createEnemy();
        } else if(this.boss == 0 && level != 0 && level%5 == 0) {
            this.createBoss();
        } else {
            this.moveEnemy();
            this.shootBullet();
            this.moveBoss();
            this.shootBomb();
        }

        this.lifeLabel.text = 'Lives : ' + life;
        this.levelLabel.text = 'Level : ' + level;
        this.SPLabel.text = 'Ultimate : ' + SP + '%';
        this.scoreLabel.text = 'Score : ' + score;

        game.physics.arcade.overlap(beams, enemies, this.hit, null, this);  
        game.physics.arcade.overlap(lasers, enemies, this.burstHit, null, this);
        game.physics.arcade.overlap(lasers, bullets, this.burstDamage, null, this);
        game.physics.arcade.overlap(bullets, player1, this.damage, null, this);
        game.physics.arcade.overlap(beams, bosses, this.hitBoss, null, this);  
        game.physics.arcade.overlap(lasers, bosses, this.burstHitBoss, null, this);
        game.physics.arcade.overlap(lasers, bombs, this.burstDamage, null, this);
        game.physics.arcade.overlap(bombs, player1, this.bigDamage, null, this);
        game.physics.arcade.overlap(auto, player1, this.getAuto, null, this);
        game.physics.arcade.overlap(help, player1, this.getHelp, null, this);
        game.physics.arcade.overlap(special, player1, this.getSpecial, null, this);
        game.physics.arcade.overlap(sbeams, enemies, this.hit, null, this);
        game.physics.arcade.overlap(sbeams, bosses, this.hitBossSpecial, null, this);  
    },

    pause: function() {
        if(game.paused == false) game.paused = true;
        else game.paused = false;
    },

    mute: function() {
        if(this.music.volume == 1) this.music.volume = 0;
        else this.music.volume = 1;

        if(this.beamSound.volume == 1) this.beamSound.volume = 0;
        else this.beamSound.volume = 1;
    },

    movePlayer: function() {
        if(this.cursor.left.isDown) {
            if(player1.x <= 40) player1.x = 30;
            else player1.x -= 10;
        }
        else if(this.cursor.right.isDown) {
            if(player1.x >= 760) player1.x = 770;
            else player1.x += 10;
        }

        if(this.cursor.up.isDown)
            if(player1.y <= 50) player1.y = 40;
            else player1.y -= 10;
        else if(this.cursor.down.isDown)
            if(player1.y >= 550) player1.y = 560;
            else player1.y += 10;
    },

    shootBeam: function() {
        if(this.spaceKey.isDown){
            if(game.time.now > beamTime) {
                beam = beams.getFirstExists(false);
    
                if(beam && burst == 0) {
                    beam.reset(player1.x, player1.y - 90);
                    beam.body.velocity.y = -400;
                    this.beamSound.play();
                    beam.emitter = game.add.emitter(player1.x, player1.y - 40, 5);
                    beam.emitter.makeParticles('pixel1');
                    beam.emitter.setYSpeed(-150, 150);
                    beam.emitter.setXSpeed(-150, 150);
                    beam.emitter.setScale(2, 0, 2, 0, 800);
                    beam.emitter.start(true, 800, null, 5);
                    beamTime = game.time.now + 150;
                    beams.forEach(function(beam) {
                        if(beam.y < 0) beam.kill();
                    });

                    if(isAuto == 1) {
                        livingTargets.length = 0;

                        enemies.forEachAlive(function(enemy) {
                            livingTargets.push(enemy);
                        });

                        bosses.forEachAlive(function(boss) {
                            livingTargets.push(boss);
                        });

                        if(livingTargets.length > 0) {
                            var random = game.rnd.integerInRange(0, livingTargets.length-1);
                            var target = livingTargets[random];
                            game.physics.arcade.moveToObject(beam, target, 2000);
                        }
                    }
                    
                    if(SP < 100) SP += 2;
                }
            }
        }
    },

    ultimateSkill: function() {
        if(burst == 0 && SP == 100) {
            if(this.shiftKey.isDown) {
                burst = 1;
                SP -= 0.3;

                laser = lasers.getFirstExists(false);
                if(laser) {
                    laser.reset(player1.x, player1.y);
                    laser.anchor.setTo(0.5, 1);
                }
            }
        } else if(burst == 1) {
            laser.kill();
            SP -= 0.3;

            laser = lasers.getFirstExists(false);
            if(laser) {
                laser.reset(player1.x, player1.y);
                laser.anchor.setTo(0.5, 1);
            }
            
            if(SP < 0) {
                SP = 0;
                burst = 0;
                laser.kill();
            }
        }
    },

    helpShoot: function() {
        if(game.time.now > helpTime) {
            beam = beams.getFirstExists(false);

            if(beam) {
                beam.reset(player1.x + 50, player1.y - 10);
                beam.body.velocity.y = -400;
                beam.emitter = game.add.emitter(player1.x + 50, player1.y + 30, 5);
                beam.emitter.makeParticles('pixel1');
                beam.emitter.setYSpeed(-150, 150);
                beam.emitter.setXSpeed(-150, 150);
                beam.emitter.setScale(2, 0, 2, 0, 800);
                beam.emitter.start(true, 800, null, 5);
                helpTime = game.time.now + 500;
                beams.forEach(function(beam) {
                    if(beam.y < 0) beam.kill();
                });
            }
        }
    },

    supportShoot: function() {
        if(game.time.now > supportTime) {
            beam = beams.getFirstExists(false);

            if(beam) {
                beam.reset(player1.x - 50, player1.y - 10);
                beam.body.velocity.y = -400;
                beam.emitter = game.add.emitter(player1.x - 50, player1.y + 30, 5);
                beam.emitter.makeParticles('pixel1');
                beam.emitter.setYSpeed(-150, 150);
                beam.emitter.setXSpeed(-150, 150);
                beam.emitter.setScale(2, 0, 2, 0, 800);
                beam.emitter.start(true, 800, null, 5);
                supportTime = game.time.now + 500;
                beams.forEach(function(beam) {
                    if(beam.y < 0) beam.kill();
                });
            }
        }
    },

    specialBeam: function() {
        if(this.ctrlKey.isDown){
            if(game.time.now > beamTime) {
                sbeam = sbeams.getFirstExists(false);
    
                if(sbeam && burst == 0) {
                    sbeam.reset(player1.x, player1.y - 90);
                    sbeam.body.velocity.y = -400;
                    this.beamSound.play();
                    sbeam.emitter = game.add.emitter(player1.x, player1.y - 40, 5);
                    sbeam.emitter.makeParticles('pixel1');
                    sbeam.emitter.setYSpeed(-150, 150);
                    sbeam.emitter.setXSpeed(-150, 150);
                    sbeam.emitter.setScale(2, 0, 2, 0, 800);
                    sbeam.emitter.start(true, 800, null, 5);
                    beamTime = game.time.now + 150;
                    sbeams.forEach(function(beam) {
                        if(sbeam.y < 0) sbeam.kill();
                    });
                    isSpecial = 0;
                }
            }
        }
    },

    createEnemy: function() {
        this.enemyNum += 1;
        if(this.enemyNum == 8) {
            this.enemyCom = 1;
            game.add.tween(player1).to({angle: 360}, 400).start();
        }

        enemy = enemies.getFirstExists(false);

        if(enemy) {
            enemy.reset(this.enemyPos, 70);
            enemy.dir = 1;
            enemy.scale.setTo(1.2, 1.2);
            enemy.anchor.setTo(0.5, 0.5);
        }

        this.enemyPos += 100;
    },

    moveEnemy: function() {
        this.enemyPos = 50;
        enemies.forEachAlive(function(enemy) {
            livingEnemies.push(enemy);

            if(enemy.x == 20) enemy.dir = 2;
            else if(enemy.x == 780) enemy.dir = 1;

            if(enemy.dir == 1) {
                if(enemy.x <= 22) enemy.x = 20;
                else enemy.x -= 2;
            } else if(enemy.dir == 2) {
                if(enemy.x >= 778) enemy.x = 780;
                else enemy.x += 2;
            }
        });
    },

    shootBullet: function() {
        if(game.time.now > bulletTime){
            livingEnemies.length = 0;
            bullet = bullets.getFirstExists(false);

            enemies.forEachAlive(function(enemy) {
                livingEnemies.push(enemy);
            });

            if(bullet && livingEnemies.length > 0) {
                var random = game.rnd.integerInRange(0, livingEnemies.length-1);
                var shooter = livingEnemies[random];
                bullet.reset(shooter.x, shooter.y + 40);
                bullet.emitter = game.add.emitter(shooter.x, shooter.y + 40, 5);
                bullet.emitter.makeParticles('pixel2');
                bullet.emitter.setYSpeed(-150, 150);
                bullet.emitter.setXSpeed(-150, 150);
                bullet.emitter.setScale(2, 0, 2, 0, 800);
                bullet.emitter.start(true, 800, null, 5);
                game.physics.arcade.moveToObject(bullet, player1, 80+(8*level));
                bulletTime = game.time.now + 500;
                bullets.forEach(function(bullet) {
                    if(bullet.y > 600) bullet.kill();
                });
            }
        }
    },

    createBoss: function() {
        this.boss = 1;

        boss = bosses.getFirstExists(false);

        if(boss) {
            bossHP = 500;
            boss.reset(game.width/2, 90);
            boss.dir = 1;
            boss.scale.setTo(1, 0.8);
            boss.anchor.setTo(0.5, 0.5);
            game.add.tween(player1).to({angle: 360}, 400).start();
        }
    },

    moveBoss: function() {
        bosses.forEachAlive(function(boss) {
            if(boss.x == 50) boss.dir = 2;
            else if(boss.x == 750) boss.dir = 1;

            if(boss.dir == 1) {
                if(boss.x <= 54) boss.x = 50;
                else boss.x -= 4;
            } else if(boss.dir == 2) {
                if(boss.x >= 746) boss.x = 750;
                else boss.x += 4;
            }
        });
    },

    shootBomb: function() {
        if(game.time.now > bombTime){
            livingBosses.length = 0;
            bomb = bombs.getFirstExists(false);

            bosses.forEachAlive(function(boss) {
                bomb.reset(boss.x, boss.y + 100);
                game.physics.arcade.moveToObject(bomb, player1, 80+(8*level));
                bombTime = game.time.now + 500;
                bombs.forEach(function(bomb) {
                    if(bomb.y > 600) bomb.kill();
                });
            });
        }
    },

    hit: function(beam, enemy) {
        beam.kill();
        enemy.kill();
        score += 5;

        beam.emitter = game.add.emitter(enemy.x, enemy.y, 10);
        beam.emitter.makeParticles('pixel1');
        beam.emitter.setYSpeed(-150, 150);
        beam.emitter.setXSpeed(-150, 150);
        beam.emitter.setScale(2, 0, 2, 0, 800);
        beam.emitter.start(true, 800, null, 10);

        this.enemyNum -= 1;

        if(this.enemyNum == 0) {
            this.enemyCom = 0;
            level += 1;
        }
    },

    burstHit: function(laser, enemy) {
        enemy.kill();
        score += 5;

        laser.emitter = game.add.emitter(enemy.x, enemy.y, 10);
        laser.emitter.makeParticles('pixel1');
        laser.emitter.setYSpeed(-150, 150);
        laser.emitter.setXSpeed(-150, 150);
        laser.emitter.setScale(2, 0, 2, 0, 800);
        laser.emitter.start(true, 800, null, 10);

        this.enemyNum -= 1;

        if(this.enemyNum == 0) {
            this.enemyCom = 0;
            level += 1;
        }
    },

    burstDamage: function(laser, bullet) {
        bullet.kill();
    },

    damage: function(player1, bullet) {
        bullet.kill();
        life -= 1;

        if(life == 0) {
            player1.kill();
            this.music.stop();
            game.state.start('over');
        }
    },

    hitBoss: function(beam, boss) {
        beam.kill();
        score += 5;
        bossHP -= 20;

        if(bossHP < 1) {
            this.boss = 0;
            boss.kill();
            level += 1;
        }
    },

    hitBossSpecial: function(beam, boss) {
        beam.kill();
        score += 5;
        bossHP = 0;

        if(bossHP < 1) {
            this.boss = 0;
            boss.kill();
            level += 1;
        }
    },

    burstHitBoss: function(laser, boss) {
        score += 5;
        bossHP -= 10;

        if(bossHP < 1) {
            this.boss = 0;
            boss.kill();
            level += 1;
        }
    },

    bigDamage: function(player1, bullet) {
        bullet.kill();
        life -= 2;

        if(life < 1) {
            player1.kill();
            this.music.stop();
            game.state.start('over');
        }
    },

    getAuto: function(auto, player1) {
        auto.kill();
        isAuto = 1;
    },

    getHelp: function(help, player1) {
        help.kill();
        isHelp = 1;

        phelp = helps.getFirstExists(false);

        if(phelp) {
            phelp.reset(player1.x+50, player1.y+50);
            phelp.scale.setTo(1.1, 1.1);
            phelp.anchor.setTo(0.5, 0.5);
        }

        support = supports.getFirstExists(false);

        if(support) {
            support.reset(player1.x-50, player1.y+50);
            support.scale.setTo(1.1, 1.1);
            support.anchor.setTo(0.5, 0.5);
        }
    },

    getSpecial: function(special, player1) {
        special.kill();
        isSpecial = 1;
    }
};

var beamTime = 0;
var bulletTime = 0;
var bombTime = 0;
var helpTime = 0;
var supportTime = 0;
var bossHP = 500;
var livingEnemies = [];
var livingTargets = [];
var livingBosses= [];
var level = 1;
var life = 20;
var SP = 0;
var burst = 0;
var score = 0;
var isAuto = 0;
var isHelp = 0;
var isSpecial = 0;