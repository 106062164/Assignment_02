var game = new Phaser.Game(800, 600, Phaser.AUTO, document.getElementById('game'));

game.state.add('boot', bootState);
game.state.add('start', startState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('over', overState);

game.state.start('boot');