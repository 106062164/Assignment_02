# Software Studio 2019 Spring Assignment 2

## Topic
* Project Name : Raiden

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle systems|10%|Y|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|N|
|Appearance|5%|
|Multi-player game|20%|N|
|Enhanced items|15%|Y|
|Boss|5%|Y|

## Website Detail Description

# 作品網址：https://106062164.gitlab.io/Assignment_02/

# Components Description : 
1. Complete game process
   There are several stages in this game. There are menu, game view, game over, and quit or play again.                                            
2. Basic rules
   Player can move and shoot. To move the player, we can press up, down, left, and right button. To shoot bullet, we can press space button. One bullet is enough to kill enemy.
   There are 8 enemies at the beginning of every stage, except the boss stage. At each single time, one enemy will shoot the bullet. Player lives will decrease by 1 if player touch enemy's bullet.
   The map will move through the game.
3. Jucify mechanisms
   The higher the level, the harder the game. When an enemy shoot a bullet, it has automatic aiming, which can be seen from this line of code.
        game.physics.arcade.moveToObject(bullet, player1, 80+(8*level));
   As we can see, the speed is 80+(8*level), so the higher the level, the faster the enemy bullet, which makes the game harder.

   Beside of a normal bullet, player also has ultimate skill. This ultimate skill can be used when the ultimate charge has already reached 100%. To activate the ultimate skill, we can press the shift button. When ultimate skill is activated, any enemy or enemy's bullet that touch our beam will die, except the boss. The ultimate skill will not kill the boss directly, but it will cause a great damage to the boss.
4. Animations
   At the beginning of every level, player will turn 360 degrees.
5. Particle systems
   There are particle systems when player/enemy shoot a bullet and when enemy touch the bullet.
6. Sound effects
   There are 2 sound effects in this game. The first one is the background music, and the second one is the sound effect when player shoot a bullet.
7. UI
   In the top of the screen, we can see the level. We also can see sound and pause icon. We can click on these icons to mute/pause the game. In the bottom of the screen, we can see the score, lives, and ultimate charge. In the beginning, the score is 0, ultimate charge is 0%, and player has 20 lives.
8. Enhanced items
   There are 3 items to help us, bullet automatic aiming, unique bullet, and little helper. To use these items, we must go to the icons which are located in the left side of the screen. Bullet automatic aiming let our bullet move automatically to enemies' spot. Unique bullet is the strongest bullet in this game and we may only use it once, even the boss will die if the boss touch this unique bullet. To shoot unique bullet, we can press the ctrl button. Little helper item gives us 2 helper beside player and help the player to shoot enemy. These helper are invincible.
9. Boss
   Boss will appear if the level%5 == 0. Player lives will decrease by 2 if player touch boss' bullet.